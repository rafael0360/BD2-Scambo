import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsuarioPage } from '../usuario/usuario';
import { Http } from '@angular/http';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

 
  url : String = "http://localhost:8090/api"

  constructor(public navCtrl: NavController) {

  }

  public login(){
    this.nav();
    
  }

  public nav(){
    this.navCtrl.push(UsuarioPage);
  }

}
