package br.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.dao.UsuarioDAO;
import br.entity.Usuario;



@Service
public class UsuarioService {

	@Autowired
	private UsuarioDAO usuarioDao;
	
	public void save(@Valid Usuario usuario) {
		this.usuarioDao.save(usuario);
	}
	
	public Usuario findOne(long id) {
		Usuario u = this.usuarioDao.findOne(id);
		return u;
	}
	
	public List<Usuario> findAll(){
		List<Usuario> usuarios = this.usuarioDao.findAll();
		return usuarios;
	}
	
	public void Delete(Usuario obj) {
		this.usuarioDao.delete(obj);
	}

	public Usuario login(String user, String pass) {
		List<Usuario> usuarios = this.usuarioDao.findAll();
		for (Usuario usuario : usuarios) {
			if(usuario.getUserName().equals(user) && usuario.getSenha().equals(pass)) {
				return usuario;
			}
		}
		return null;
	}
	
}
