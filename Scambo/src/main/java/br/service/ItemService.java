package br.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.dao.ItemDAO;
import br.entity.Item;
import br.entity.Usuario;

@Service
public class ItemService {

	@Autowired
	private ItemDAO itemDAO;
	
	
	public void save(@Valid Item item) {
		this.itemDAO.save(item);
	}
	
	public Item findOne(long id) {
		Item u = this.itemDAO.findOne(id);
		return u;
	}
	
	public List<Item> findAll(){
		List<Item> itens = this.itemDAO.findAll();
		return itens;
	}
	
	public void Delete(Item obj) {
		this.itemDAO.delete(obj);
	}
	
}
