package br.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.dao.PropostaDAO;
import br.entity.Proposta;
import br.entity.Publicacao;

@Service
public class PropostaService {
	

	private PropostaDAO propostaDAO = new PropostaDAO();
	
	
	public void save(@Valid Proposta pub) {
		this.propostaDAO.save(pub);
	}
	
	public Proposta findOne(long id) {
		Proposta u = this.propostaDAO.findOne(id);
		return u;
	}
	
	public List<Proposta> findAll(){
		List<Proposta> proposta = this.propostaDAO.findAll();
		return proposta;
	}
	
	public void Delete(Proposta obj) {
		this.propostaDAO.delete(obj);
	}
}
