package br.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.dao.PublicacaoDAO;
import br.entity.Item;
import br.entity.Publicacao;

@Service
public class PublicacaoService {

	@Autowired
	private PublicacaoDAO publicacaoDAO;
	
	
	public void save(@Valid Publicacao pub) {
		this.publicacaoDAO.save(pub);
	}
	
	public Publicacao findOne(long id) {
		Publicacao u = this.publicacaoDAO.findOne(id);
		return u;
	}
	
	public List<Publicacao> findAll(){
		List<Publicacao> itens = this.publicacaoDAO.findAll();
		return itens;
	}
	
	public void Delete(Publicacao obj) {
		this.publicacaoDAO.delete(obj);
	}
	
}
