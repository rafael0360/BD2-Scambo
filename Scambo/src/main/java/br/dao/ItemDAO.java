package br.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.entity.Item;
import br.entity.Usuario;

@Repository
public class ItemDAO extends DAO{

	public void save(Item item) {
		this.begin();
		this.getEntityManager().persist(item);
		this.commit();
	}

	public Item findOne(long id) {
		Item i;
		this.begin();
		i = this.getEntityManager().find(Item.class, id);
		this.commit();
		return i;
	}

	public List<Item> findAll() {
		
		List<Item> list;
		this.begin();
		Query query = this.getEntityManager().createQuery("select i from itens i");
		list = query.getResultList();
		this.commit();
		return list;
		
	}
	
	public void delete(Item obj) {
		this.begin();
		this.getEntityManager().remove(obj);
		this.commit();
	}
	
	
	
}
