package br.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.entity.Proposta;
import br.entity.Usuario;

@Repository
public class PropostaDAO extends DAO{
	
	public void save(Proposta proposta) {
		this.begin();
		this.getEntityManager().persist(proposta);
		this.commit();
	}

	public Proposta findOne(long id) {
		Proposta u;
		this.begin();
		u = this.getEntityManager().find(Proposta.class, id);
		this.commit();
		return u;
	}

	public List<Proposta> findAll() {
		
		List<Proposta> list;
		this.begin();
		Query query = this.getEntityManager().createQuery("select p from propostas p");
		list = query.getResultList();
		this.commit();
		return list;
		
	}
	
	public void delete(Proposta obj) {
		this.begin();
		this.getEntityManager().remove(obj);
		this.commit();
	}


}
