package br.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class DAO {
	
	private EntityManagerFactory entityManagerFactory;
	
	private EntityManager entityManager;
	
	public DAO() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("webApp");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void begin() {
		this.entityManager.getTransaction().begin();
	}
	
	public void commit() {
		this.entityManager.getTransaction().commit();
	}
	
	public void rollback() {
		this.entityManager.getTransaction().rollback();
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	
	
}
