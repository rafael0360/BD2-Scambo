package br.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.entity.Usuario;


@Repository
public class UsuarioDAO extends DAO{
	
	public void save(Usuario usuario) {
		this.begin();
		this.getEntityManager().persist(usuario);
		this.commit();
	}

	public Usuario findOne(long id) {
		Usuario u;
		this.begin();
		u = this.getEntityManager().find(Usuario.class, id);
		this.commit();
		return u;
	}

	public List<Usuario> findAll() {
		Query query = this.getEntityManager().createQuery("select u from usuarios u");
		List<Usuario> list;
		this.begin();
		list = query.getResultList();
		this.commit();
		return list;
		
	}
	
	public void delete(Usuario obj) {
		this.begin();
		this.getEntityManager().remove(obj);
		this.commit();
	}

}
