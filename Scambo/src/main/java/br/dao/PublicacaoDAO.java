package br.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.entity.Item;
import br.entity.Publicacao;

@Repository
public class PublicacaoDAO extends DAO {

	public void save(Publicacao pub) {
		this.begin();
		this.getEntityManager().persist(pub);
		this.commit();
	}

	public Publicacao findOne(long id) {
		Publicacao i;
		this.begin();
		i = this.getEntityManager().find(Publicacao.class, id);
		this.commit();
		return i;
	}

	public List<Publicacao> findAll() {
		
		List<Publicacao> list;
		this.begin();
		Query query = this.getEntityManager().createQuery("select i from publicacoes i");
		list = query.getResultList();
		this.commit();
		return list;
		
	}
	
	public void delete(Publicacao obj) {
		this.begin();
		this.getEntityManager().remove(obj);
		this.commit();
	}
	
}
