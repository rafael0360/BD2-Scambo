package br.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.entity.Usuario;
import br.service.UsuarioService;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/usuario/{id}")
	public ResponseEntity<Usuario> getUsuario(@PathVariable long id){
		Usuario u = this.usuarioService.findOne(id);
		return new ResponseEntity<Usuario>(u, HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/usuario")
	public ResponseEntity<String> saveUsuario(@RequestBody @Valid Usuario usuario){
		this.usuarioService.save(usuario);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@GetMapping("/usuario")
	public ResponseEntity<List<Usuario>> getAllUsuario(){
		List<Usuario> usuarios = this.usuarioService.findAll();
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/usuario/login")
	public ResponseEntity<Usuario> login(@RequestParam String user, @RequestParam String pass){
		Usuario usuario = this.usuarioService.login(user, pass);
		if(usuario == null) {
			return new ResponseEntity<Usuario>(usuario, HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<Usuario>(usuario, HttpStatus.ACCEPTED);
		}
	}
	
}
