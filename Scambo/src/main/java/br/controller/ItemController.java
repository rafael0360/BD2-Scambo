package br.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.entity.Item;
import br.service.ItemService;

@RestController
@RequestMapping("/api")
public class ItemController {

	@Autowired
	ItemService itemService;
	
	@GetMapping("item")
	public ResponseEntity<List<Item>> getAllItem(){
		List<Item> lista = this.itemService.findAll();
		return new ResponseEntity<List<Item>>(lista, HttpStatus.ACCEPTED);
	}
	
}
